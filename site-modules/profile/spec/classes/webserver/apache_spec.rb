require 'spec_helper'

describe 'profile::webserver::apache' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:node) { 'testhost.example.com' }
      context "with params" do
      let(:params) { { 
        'apache_mods' => [ 'x', 'y', 'z' ],
        'virtualhosts' => [
          { 'servername' => 'teste1.example.com',
            'port' => '80',
            'docroot' => '/var/www/html',
          },
          { 'servername' => 'teste2.example.com',
            'port' => '443',
            'docroot' => '/var/www/html2',
          },
        ]
      } }
      it { is_expected.to contain_class('apache').with({
        'default_mods' => [ 'x', 'y', 'z' ],
        'default_vhost' => false,
      })}
      it { is_expected.to contain_apache__vhost('teste1.example.com').with({
        'servername' => 'teste1.example.com',
        'port' => '80',
        'docroot' => '/var/www/html',
      })}
      end
      context "with no params" do
        it { is_expected.to contain_class('apache').with({
            'default_mods' => true,
            'default_vhost' => true,
        })}
        it { is_expected.to contain_apache__vhost('default') }
      end
      it { is_expected.to compile }
    end
  end
end
