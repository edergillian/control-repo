require 'spec_helper'

describe 'profile::base::myntp' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) { {'servidor' => [ 'teste.ntp.nt' ] } }
      it { is_expected.to contain_class('ntp').with({
        'servers' => [ 'teste.ntp.nt' ],
        'restrict' => ['127.0.0.1'],
        'burst' => true,
      })}
      it { is_expected.to compile }
    end
  end
end
