# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include profile::webserver::apache
class profile::webserver::apache (
  Variant[Boolean, Array] $apache_mods = true,
  Optional[Array[Hash]] $virtualhosts = undef,
) {

  if is_array($virtualhosts) {
    $_default_vhost = false
  }
  else {
    $_default_vhost = true
  }

  class {'::apache':
    default_mods  => $apache_mods,
    default_vhost => $_default_vhost,
  }

  if ! $_default_vhost {
    $virtualhosts.each |Hash $vhosts|  {
      ::apache::vhost { $vhosts['servername']:
        servername => $vhosts['servername'],
        port       => $vhosts['port'],
        docroot    => $vhosts['docroot'],
      }
    }
  }
}
