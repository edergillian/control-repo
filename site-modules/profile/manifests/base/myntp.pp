# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include profile::base::myntp
class profile::base::myntp (
  Array $servidor = [ 'timeserver' ]
) {
  class {'ntp':
    servers  => $servidor,
    restrict => ['127.0.0.1'],
    burst    => true,
  }
}
